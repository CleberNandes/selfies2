/* jshint esversion: 6 */
import React, { Component } from "react";
import Pubsub from 'pubsub-js'; 

export default class Header extends Component {

  pesquisa(e){
    e.preventDefault();
    fetch(`https://instalura-api.herokuapp.com/api/public/fotos/${this.search.value}`)
      .then(response => response.json())
      .then(fotos => {
        Pubsub.publish('update-search',fotos);
      });

  }

  render() {
    return (
      <header className="header container">
        <h1 className="header-logo">SelfieS2</h1>

        <form className="header-busca" onSubmit={this.pesquisa.bind(this)}>
          <input
            type="text"
            name="search"
            placeholder="Pesquisa"
            className="header-busca-campo" ref={input => this.search = input}
          />
          <input type="submit" value="Buscar" className="header-busca-submit" />
        </form>
        <nav>
          <ul className="header-nav">
            <li className="header-nav-item">
              <a href="#">
                ♡
                {/*                 ♥ */}
                {/* Quem deu like nas minhas fotos */}
              </a>
            </li>
          </ul>
        </nav>
      </header>
    );
  }
}
