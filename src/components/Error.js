/* jshint esversion: 6 */
import React, {Component} from 'react';
import Header from "./Header";

export default class Error extends Component {

    render(){
        return <div id="root">
            <div className="main">
              <Header />
              <h1>Erro 404</h1>
              <h4>Não foram encontrados sinais de vida.</h4>
              <h3>Url: {window.location.href}</h3>
            </div>
          </div>;
    }
}