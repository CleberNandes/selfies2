/* jshint esversion: 6 */
import React, { Component } from "react";
import FotoItem from "./FotoItem";
import Pubsub from 'pubsub-js';
import ReactCSSTransitionGroup from "react-addons-css-transition-group";

export default class Timeline extends Component {

    constructor(props) {
        super(props);        
        this.state = {fotos:[]};
        this.login = this.props.login;        
    }

    componentWillMount() {
        Pubsub.subscribe('update-search',(topico,fotos) => {
            this.setState({fotos});      
        });
    }

    carregaFotos(){
        let urlPerfil;
        if (this.login === undefined) {
          urlPerfil = `https://instalura-api.herokuapp.com/api/fotos?X-AUTH-TOKEN=
                    ${localStorage.getItem("auth-token")}`;
        } else {
          urlPerfil = `https://instalura-api.herokuapp.com/api/public/fotos/${this.login}`;
        }
        // abaixo a crase como aspas, deixa o js ser concatenado de forma dinamica
        fetch(urlPerfil)
        .then(response => response.json())
        .then(fotos => {
            this.setState({ fotos });
          });
    }
    
    componentDidMount(){
        this.carregaFotos();
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.login !== undefined){
            this.login = nextProps.login;
            this.carregaFotos();
        }
    }

    render() {
        return (
            <div className="fotos container">
                <ReactCSSTransitionGroup 
                    transitionName="timeline"
                    transitionEnterTimeout={500}
                    transitionLeaveTimeout={300}>
                    {
                        this.state.fotos.map(foto => (
                    <FotoItem key={foto.id} foto={foto} />
                    ))
                    }
                </ReactCSSTransitionGroup>
            </div>
        );
    }
}
