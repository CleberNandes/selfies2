/* jshint esversion: 6*/
import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';

export default class Login extends Component 
{
    constructor(props){
        super(props);
        this.state = {msg:props.msg};
    }

    submitLogin(event){
        event.preventDefault();
        var requestInfo = {
            method: 'POST',
            body:   JSON.stringify({
                login: this.login.value,
                senha: this.senha.value
            }),
            headers: new Headers({
                'Content-type':'application/json'
            })
        };

        fetch('https://instalura-api.herokuapp.com/api/public/login',requestInfo)
            .then(response => {
                console.log(response);
                if(response.ok){
                    return response.text();
                } 
                throw new Error('Não foi possivel fazer o login');

            }).then(token => {
                // caso log seta um token no localStorage
                localStorage.setItem('auth-token',token);
                window.location.replace('/timeline')
                /* render(<Redirect to="/timeline"/>); */
                console.log(token);                
            }).catch(error => {
                console.log(error.message);
                this.setState({msg:error.message});
            });
    }

    render(){
        return (
            <div className="login-box">
                <h1 className="header-logo">Instalura</h1>
                <form onSubmit={this.submitLogin.bind(this)}>
                <input type="text" ref={input => (this.login = input)} />
                <input type="password" ref={input => (this.senha = input)} />
                <input type="submit" value="login" />
                </form>
                <span>{this.state.msg}</span>
            </div>);
    }
}

/* alots senha= 123456 */