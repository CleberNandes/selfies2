/* jshint esversion: 6 */
import React from 'react';
import ReactDOM from 'react-dom';
import App from "./App";
import Login from "./components/Login";
import Error from "./components/Error";
import Logout from './components/Logout';
import registerServiceWorker from './registerServiceWorker';
import './css/reset.css';
import './css/timeline.css';
import './css/login.css';
import {BrowserRouter as Router, Route, Switch, Redirect, matchPath
} from 'react-router-dom';

function authentication(nextState) 
{   
  const hasAttribute = nextState.match.params.login;
  
  if (!hasAttribute && localStorage.getItem("auth-token") === null) {
    /* nextState.history.replace("/"); */
    window.history.pushState("", "", "/");
    return <Login />;
  }
  if (!hasAttribute) {
    window.history.pushState("", "", "/timeline");
  }

  return <App login={hasAttribute} />;
}

ReactDOM.render((
  <Router>
    <Switch>
      <Route exact path="/" render={authentication} />
      <Route path="/timeline/:login?" render={authentication} />
      <Route path="/logout" component={Logout} />
      <Route path="/*" component={Error} />
    </Switch>
  </Router>),
  document.getElementById("root")
);
registerServiceWorker();
